package com.example.canvasee.data;

import com.example.canvasee.R;

public class Topic {
    public String title;
    public String author;
    public String time;
    public String day;
    public int photo;


    public Topic(String title, String author, String time, String day,int photo) {
        this.title = title;
        this.author = author;
        this.time = time;
        this.day = day;
        this.photo = photo;
    }
}
