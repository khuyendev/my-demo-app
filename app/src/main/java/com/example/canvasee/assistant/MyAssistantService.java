package com.example.canvasee.assistant;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.service.voice.VoiceInteractionService;
import android.util.Log;

public class MyAssistantService extends VoiceInteractionService {

    static final String TAG = "MainInteractionService";
    @Override
    public void onCreate(){
        super.onCreate();
        Application myApp = this.getApplication();
        myApp.registerOnProvideAssistDataListener(
                new Application.OnProvideAssistDataListener() {
                    @Override
                    public void onProvideAssistData(Activity activity, Bundle bundle) {
                    }
                }
        );
        Log.v("@@","MyAssistant_Service onCreate() succuss");
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        super.onStartCommand(intent,flags,startId);
        return START_STICKY;
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
    }


}
