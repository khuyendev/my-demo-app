package com.example.canvasee.assistant;

import android.os.Bundle;
import android.service.voice.VoiceInteractionSession;
import android.service.voice.VoiceInteractionSessionService;
import android.util.Log;

public class MyAssistantSessionService extends VoiceInteractionSessionService {

    @Override
    public VoiceInteractionSession onNewSession(Bundle bundle) {
        Log.v("@@","MyAssistant_Session_Service call MyAssistantSession(), it make new Session.");
        return new MyAssistantSession(this);
    }

}