package com.example.canvasee.assistant;

import android.app.assist.AssistContent;
import android.app.assist.AssistStructure;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.service.voice.VoiceInteractionSession;
import android.view.View;

import com.example.canvasee.ui.MainActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class MyAssistantSession extends VoiceInteractionSession {

    ArrayList list = new ArrayList();
    HashMap<Integer,CharSequence> map = new HashMap<>();
    public MyAssistantSession(Context context) {
        super(context);
    }

    public void findNode(AssistStructure.ViewNode node){
        for(int i=0;i<node.getChildCount();i++){
            if(node.getChildAt(i).getText() != null && node.getChildAt(i).getVisibility() == View.VISIBLE ) {
                list.add(node.getChildAt(i).getText());
            }

            if(node.getChildAt(i).getVisibility() != 0)
                continue;
            if(node.getChildAt(i).getChildCount() != 0){
                this.findNode(node.getChildAt(i));
            }
        }
    }
    public void print_map(HashMap<Integer,CharSequence> map){
        Iterator<Integer> iterator = map.keySet().iterator();
        while(iterator.hasNext()){
            Integer key = iterator.next();
            System.out.println("node_ID : " + key + "  type  node_ID_ENTRY : " + map.get(key));
        }
    }
    @Override
    public void onHandleAssist(Bundle data, AssistStructure structure, AssistContent content) {
        super.onHandleAssist(data, structure, content);
        for(int i=0;i<structure.getWindowNodeCount();i++){
            findNode(structure.getWindowNodeAt(i).getRootViewNode());
        }
        Context context = getContext();
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putStringArrayListExtra("data",list);
        intent.putExtra("key","key");
        context.startActivity(intent);
        finish();
    }

    @Override
    public View onCreateContentView() {
        return null;
    }

}
