package com.example.canvasee.ui.data;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.canvasee.R;
import com.example.canvasee.ui.DataAdapter;

import java.util.ArrayList;


public class DataFragment extends Fragment {
    private DataAdapter dataAdapter;
    private RecyclerView recyclerView;

    private ArrayList<String> strings;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        strings = getActivity().getIntent().getStringArrayListExtra("data");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_data, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        dataAdapter = new DataAdapter(strings);
        recyclerView.setAdapter(dataAdapter);
    }
}
