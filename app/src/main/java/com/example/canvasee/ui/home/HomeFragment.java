package com.example.canvasee.ui.home;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.canvasee.R;
import com.example.canvasee.data.Topic;
import com.example.canvasee.ui.DataAdapter;
import com.example.canvasee.ui.TopicAdapter;

import java.util.ArrayList;
import java.util.List;


public class HomeFragment extends Fragment {
    private RecyclerView mRecyclerViewTopic;
    private TopicAdapter topicAdapter;
    private List<Topic> topics = new ArrayList<>();
    private ArrayList<String> strings;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        strings = getActivity().getIntent().getStringArrayListExtra("data");
        if (strings == null) {
            strings = new ArrayList<>();
        }
        initFakeData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        topicAdapter = new TopicAdapter(topics);
        mRecyclerViewTopic = view.findViewById(R.id.recyclerViewTopic);
        mRecyclerViewTopic.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerViewTopic.setAdapter(topicAdapter);
    }

    void initFakeData() {
        if (strings.size() > 0) {
            if (!TextUtils.isEmpty(String.valueOf(strings.get(0)))) {
                String result = String.valueOf(strings.get(0));
                String title = result.split("- Google Search")[0];
                topics.add(new Topic(title, "username", "12:12", "12/12/2012", R.drawable.img1));
                topics.add(new Topic(title, "username", "12:12", "12/12/2012", R.drawable.img1));
            }
        }
        for (int i = 1; i < strings.size(); i++) {
            if (!TextUtils.isEmpty(String.valueOf(strings.get(i)))) {
                topics.add(new Topic(String.valueOf(strings.get(i)), "username", "12:12", "12/12/2012", R.drawable.img2));
            }
        }
    }
}
