package com.example.canvasee.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.canvasee.R;
import com.example.canvasee.data.Topic;

import java.util.List;

public class TopicAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int VIEW_ONE = 1;
    private static final int VIEW_TWO = 2;
    private List<Topic> topics;

    public TopicAdapter(List<Topic> topics) {
        this.topics = topics;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return VIEW_ONE;
            case 1:
                return VIEW_ONE;
            default:
                return VIEW_TWO;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = null;
        switch (i) {
            case VIEW_ONE:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_topic_type_1, viewGroup, false);
                return new TopicViewHolder(v);
            case VIEW_TWO:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_topic_type_2, viewGroup, false);
                return new UserViewHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof TopicViewHolder) {
            ((TopicViewHolder) viewHolder).bindData(topics.get(i));
        }
        if (viewHolder instanceof UserViewHolder) {
            ((UserViewHolder) viewHolder).bindData(topics.get(i));
        }
    }

    @Override
    public int getItemCount() {
        return topics != null ? topics.size() : 0;
    }

    public class TopicViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView author;
        TextView day;
        TextView time;
        ImageView photo;

        public TopicViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            author = itemView.findViewById(R.id.author);
            day = itemView.findViewById(R.id.day);
            time = itemView.findViewById(R.id.time);
            photo = itemView.findViewById(R.id.photo);
        }

        public void bindData(Topic topic) {
            title.setText(topic.title);
            author.setText(topic.author);
            day.setText(topic.day);
            time.setText(topic.time);
            photo.setImageResource(topic.photo);
        }
    }

    public class UserViewHolder extends RecyclerView.ViewHolder {

        TextView author;
        TextView day;
        TextView time;
        ImageView photo;
        public UserViewHolder(@NonNull View itemView) {
            super(itemView);

            author = itemView.findViewById(R.id.author);
            day = itemView.findViewById(R.id.day);
            time = itemView.findViewById(R.id.time);
            photo = itemView.findViewById(R.id.photo);
        }

        public void bindData(Topic topic) {
            author.setText(topic.author);
            day.setText(topic.day);
            time.setText(topic.time);
            photo.setImageResource(topic.photo);
        }
    }
}
