package com.example.canvasee.ui;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import com.example.canvasee.R;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.DataViewHolder> {
    private List<String> strings;

    public DataAdapter(List<String> strings) {
        this.strings = strings;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_data, viewGroup, false);
        return new DataViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder dataViewHolder, int i) {
        dataViewHolder.bindData(String.valueOf(strings.get(i)));
    }

    @Override
    public int getItemCount() {
        return strings != null ? strings.size() : 0;
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        TextView data;

        public DataViewHolder(@NonNull View itemView) {
            super(itemView);
            data = itemView.findViewById(R.id.data);
        }

        public void bindData(String s) {
            data.setText(s);
        }
    }
}
